## Xiaomi Firmware Packages For Redmi Note 4 Global (mido)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 309 | HMNote4XGlobal | Xiaomi Redmi Note 4 Global | mido |

### XDA Support Thread For sagit:
[Go here](https://forum.xda-developers.com/redmi-note-4/xiaomi-redmi-note-4-snapdragon-roms-kernels-recoveries--other-development/firmware-xiaomi-redmi-note-4-t3760917)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
